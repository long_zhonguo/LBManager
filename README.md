# Ios投屏技术分享之乐播投屏精装版二次封装

#### 介绍
简单的投屏封装，单利模式全局声明，调用更为方便，数据一致性更强更稳定

###前言：
先简单介绍一下投屏，投屏分为两个模式。
- #####1·推送模式
![21575943980_.pic.jpg](https://upload-images.jianshu.io/upload_images/6884657-f95fbf6489640626.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

主要用于投屏视频和音乐，投屏之后手机可以关闭或聊微信，电视不会停止播放。玩手机看电视两不误。最常见是视频软件里的tv按钮。

投屏推送的原理：原理就是，让手机与电视连接同一个wifi后，通过投屏协议传输数据。（就好像蓝牙一样的一个专门通道）。点击投屏按钮，手机就开始搜索wifi内有没有投屏广播服务。

手机搜索到电视之后，手机会发送一个视频地址给电视，电视收到地址后，开始播放。投屏后，手机可以控制电视的进度，暂停，下一集，音量之类的指令。

投屏协议有DLNA协议，airplay推送协议，lelink协议（包括以上两者）。

- #####2·镜像模式
![大使馆.png](https://upload-images.jianshu.io/upload_images/6884657-f61dfacbdf231c3a.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

就是投屏手机屏幕或者投屏电脑屏幕；就是同屏显示。主要用于投屏手机PPT、手机桌面、吃鸡王者荣耀等手游；投屏淘宝微，信，抖音等暂时投屏功能的APP。苹果手机的屏幕镜像，安卓手机的多屏互动和乐播投屏手机版的镜像功能，都属于镜像模式。

原理：一样依靠局域网wifi通道，手机搜索到电视之后，开始镜像后，手机会不断截屏就是录屏。手机一边录屏，一般发送给电视。速度超快每秒60帧以上，电视收到之后再展现出来，就成了同屏功能了。

镜像协议有：miracast（难用，被抛弃，不是采用wifi传输），airplay镜像（苹果的，wifi局域网传输）。lelink镜像（乐播投屏的，wifi局域网传输，也支持airplay镜像）。

好了废话不多说，简单的先了解到这里，以下封装由于项目中的时间成本的问题就简单集成了[乐播sdk(点击这里去官网查看集成规则)](https://cloud.hpplay.cn:9889/dev/#https://cdn.hpplay.com.cn/doc/sdk/source/iOS/_book/)

会用cocoapod集成接着往下看
pod 'LBLelinkKit'

- 下面咱们就讲讲我这个封装都干了什么

![1575945174744.jpg](https://upload-images.jianshu.io/upload_images/6884657-172f516e3fe3d123.jpg?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
你不用不知道，一用下一跳，组件化开发的优势无与伦比的展现出来，一个一个功能的分离导致用起来及其不方便，用过的都知道，那么接下来福利时间，pod一键导入再加上我的封装简直了（让我们愉快的投起屏来吧！）。

```
@protocol DLNADelegate <NSObject>

@optional
/**
DLNA局域网搜索设备结果
@param devicesArray  搜索到的设备 名称
*/
- (void)searchDLNAResult:(NSArray *)devicesArray;
/**
搜索失败
*/
- (void)searchDLNAError:(NSError *)error;

/**
投屏成功开始播放
*/
- (void)dlnaStartPlay;
/**
退出投屏成功
*/
- (void)dlnaEndPlay;
/**
投屏失败
*/
- (void)dlnaErrorPlay;
/**
播放状态代理回调
*/
- (void)dlna_lelinkPlayer:(LBLelinkPlayer *)player playStatus:(LBLelinkPlayStatus)playStatus;
/**
播放进度信息回调
*/
- (void)dlna_lelinkPlayer:(LBLelinkPlayer *)player progressInfo:(LBLelinkProgressInfo *)progressInfo;
// 播放错误代理回调，根据错误信息进行相关的处理
- (void)dlna_lelinkPlayer:(LBLelinkPlayer *)player onError:(NSError *)error;


@end
@interface LBManager : NSObject
@property(nonatomic,weak)id<DLNADelegate> delegate;

@property (nonatomic,strong) LBLelinkBrowser * lelinkBrowser;
@property (nonatomic,strong) LBLelinkConnection * lelinkConnection;
@property (nonatomic,strong) LBLelinkPlayer * player;
@property (nonatomic,strong) LBLelinkPlayerItem * item ;
@property (nonatomic,strong) NSArray<LBLelinkService *> *services;
//初始化配置
+(void)newLBManager;

+ (LBManager *)sharedDLNAManager;

/**
搜索投屏服务
*/
-(void)searchService;
/**
结束搜索投屏服务
*/
-(void)endService;

#pragma mark - 建立连接
/**
初始化建立链接
@param index 投放设备list下标
*/
-(void)startLBLelinkConnection:(NSInteger)index;
/**
初始化建立链接
@param service 服务信息
*/
-(void)startLBLelinkConnectionService:(LBLelinkService *)service;

/**
开始链接
*/
-(void)startConnect;
/**
结束连接
*/
-(void)endConnect;



#pragma mark - 播放控制
/**
初始化播放
@param playUrl 播放地址
*/
-(void)setLBLelinkPlayerItemPlayUrl:(NSString *)playUrl;
/**
初始化播放
@param playUrl 播放地址
@param startPosition 播放进度 （秒）
*/
-(void)setLBLelinkPlayerItemPlayUrl:(NSString *)playUrl startPosition:(NSInteger)startPosition;
/**
进度调节
@param startPosition 调节进度的位置，单位秒
*/
-(void)setStartPosition:(NSInteger)startPosition;
//判断是音频还是视频
-(BOOL)isPlayType:(NSString *)playUrl;

/**
播放
*/
-(void)play;
/**
暂停播放
*/
- (void)pause;

/**
继续播放
*/
- (void)resumePlay;

/**
进度调节

@param seekTime 调节进度的位置，单位秒
*/
- (void)seekTo:(NSInteger)seekTime;

/**
退出播放
*/
- (void)stop;

/**
设置音量值

@param value 音量值，范围0 ~ 100
*/
- (void)setVolume:(NSInteger)value;

/**
增加音量
*/
- (void)addVolume;

/**
减小音量
*/
- (void)reduceVolume;


@end
```
- 使用方法如下
```
//搜索服务设备
self.dlnaManager = [LBManager sharedDLNAManager];
self.dlnaManager.delegate = self;
[self.dlnaManager searchService];
[self.dlnaView yl_showLoadingHud:@"正在搜索设备..."];
//结束投屏
[self.dlnaManager endService];

#pragma mark - DLNA-Delegate
- (void)searchDLNAResult:(NSArray *)devicesArray{
if (devicesArray.count != 0) {
NSLog(@"发现设备");
}else{
NSLog(@"没有发现设备");
}
}

- (void)dlnaStartPlay{
NSLog(@"投屏成功 开始播放");
[self.dlnaManager endService];
//    [self.dlnaManager setLBLelinkPlayerItemPlayUrl:self.playUrl];//推送播放资源地址Url

}
// 播放状态代理回调
-(void)dlna_lelinkPlayer:(LBLelinkPlayer *)player playStatus:(LBLelinkPlayStatus)playStatus{
switch (playStatus) {
case LBLelinkPlayStatusCommpleted:
{

}
break;
case LBLelinkPlayStatusLoading:
{

}
break;
case LBLelinkPlayStatusPlaying:
{

}
break;
case LBLelinkPlayStatusPause:
{

}
break;
case LBLelinkPlayStatusError:
{

}
break;
default:
break;
}
}
//播放进度
-(void)dlna_lelinkPlayer:(LBLelinkPlayer *)player progressInfo:(LBLelinkProgressInfo *)progressInfo{
//self.currentTimeLabel.text = [NSString stringWithTime:progressInfo.currentTime];
//self.progressSlider.maximumValue = progressInfo.duration;
//self.progressSlider.value = progressInfo.currentTime;
//self.totalTimeLabel.text = [NSString stringWithTime:progressInfo.duration];
}
-(void)gs_outDLNAPlay{
NSLog(@"退出投屏 停止播放");
[self.dlnaManager stop];
[self.dlnaManager pause];
[self.dlnaManager endService];
[self dismissViewControllerAnimated:NO completion:nil];
}
```
写的东西呢！仅供大家参考，如有误区还望大家及时指正。-最后还是希望能帮助到看到这篇文章的你😊。


#### 码云特技
1.   [乐播投屏精装版](https://www.jianshu.com/p/0fcac7bbd1e1)

